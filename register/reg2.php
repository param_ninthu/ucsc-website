<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="style.css">
  <title>Register</title>
</head>
<body>
<?php  

define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_NAME', 'Webreg');

$conn = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);

if($conn)
    { 
      function validate_input($data)
      {
          $data =trim($data);
          $data=stripslashes($data);
          $data=htmlspecialchars($data);

          return $data;
      }
      
      function strlength($str)
      {
          $length = strlen($str);
          if($length >10)
              return "Password Should less than 10 Characters";
          elseif($length<5)
              return "Password should contain more than 5 Characters";   
              
      }

            if(isset($_POST['sub']))
            {
                $Uname =$_POST['uname'];
                $Email =$_POST['email'];
                $Pass =$_POST['psw'];
                $RePass =$_POST['repsw'];
                $Phone =$_POST['phone'];

                 /* Variables for validation  */
                 $user_boolean = false;
                 $email_boolean = false;
                 $pass_boolean = false;
                 $phone_boolean = false;
                 $uerr = " ";
                 $errr =" ";
                 $perr= " ";
                 $reperr=" ";
                 $pherr =" ";
                 // Username Validation
                 if(empty($Uname))
                     {
                       $uerr = "Username is required ..";  
                       $user_boolean =false;
                     }
                 else
                     {
                         $user_boolean=true;
                     }  
                     
                     // Email Validation
                 if(empty($Email))
                     {
                         $errr = "Email is required";
                         $email_boolean = false;
                     }
                 elseif(!filter_var($Email,FILTER_VALIDATE_EMAIL))
                     {
                         $errr ="Invalid Email Address";
                         $email_boolean=false;
                     }
                 else
                     {
                         $email_boolean=true;
                     }
 
                     // Password Validation
 
                 $passlength =strlength($Pass);
 
                    if(empty($Pass))
                     {
                         $perr = "Password field should be filled";
                         $pass_boolean=false;
                     }
                    elseif($passlength)
                     {
                         $perr=$passlength;
                         $pass_boolean=true;
                     }
                    else
                     {
                         $pass_boolean = true;
                     }
 
                     // Re Password Validation
 
                     if(empty($RePass))
                     {
                         $reperr = "Password field should be filled";
                         $pass_boolean=false;
                     }    
                     elseif($RePass != $Pass)
                         {
                             $reperr = " Password is not match ";
                             $pass_boolean =false;
 
                         }
                    else
                     {
                        $pass_boolean=true;
                     }
                        
 
                     //  Phone number validation
 /*
                     $phone_bool =false;
                     for($x=0 ;$x<strlen($Phone);$x++)
                         {
                             if($Phone[$x] <= 9 && $Phone[$x] >= 0 )
                                 {
                                   $phone_bool =true;
                                 }
                             else 
                                 {
                                     $phone_bool =false;
                                     break;
                                 }
                         }
                     if($phone_bool==false)
                         {
                             $reperr="Invalid Characters ";
                         }
                     else
                         {
                             $phone =validate_user($Phone);
                         }
              

*/
                    $phone_boolean =true;

              


              if($pass_boolean && $phone_boolean && $user_boolean && $email_boolean)
              {

                $sqlfinal = "SELECT User_Name , PASS FROM reg WHERE User_Name='$Uname' AND Email='$Email'  ";
               // $stmtinsert = $conn -> prepare($sqlreg);
                $result_final =mysqli_query($conn ,$sqlfinal);
                {
                  if(mysqli_num_rows($result_final)==1)
                    {
                        echo "<script>alert('You Are Already Registered!')  </script>";
                        exit();
                    }
                    else
                    {
                        $sqlreg = "INSERT INTO reg VALUES('$Uname','$Email','$Pass','$Phone')";
               
                        // $stmtinsert = $conn -> prepare($sqlreg);
                         $result =mysqli_query($conn , $sqlreg);
                 
                 
                         if($result)
                             {
                                 echo "<script> alert('Successful Registration...!') </script>";
                                 header("Location: log.php");
                             }
                         else
                             {
                               echo "<script> alert('Error in  Registration...!') </script>";
                             }
                    }
                }

       

            }
          }
    }

?>  
  <a href="../index.html">
    <img src="logo-nav.png" width="60px" class="logo">
  </a>
  <div class="register">
    <h1> User Signup</h1>
    <h2>Sign up for get unlimited tech support!</h2>
    <form action="reg2.php" name="reg-form" method="POST">
      <label>Username</label>
      <input type="text" placeholder="Enter your username" name="uname" id="uname">      <label>Email</label>
      <input type="email" placeholder="Enter your email" name="email" id="email">
      <label>Password</label>
      <input type="password" placeholder="Enter your password" name="psw" id="psw">
      <label>Re-Enter Password</label>
      <input type="password" placeholder="Re-Enter your password" name="repsw" id="repsw">
      <label>Phone Number</label>
      <input type="text" placeholder="Enter your phone number" name="phone" id="phone">
      <button type="submit" name="sub">Register</button>
      <h3>Already have an account? <a href="../login/log.php">Sign in</a></h3>
    </form>
  </div>

  </body>
</html>